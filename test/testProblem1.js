//Access the dataset and the function to find all web developers
const data = require("../js_drill_2.cjs");
const findAllWebDevelopers = require("../problem1");

try {
    // Call the findAllWebDevelopers function with the dataset
  let developers = findAllWebDevelopers(data);

  console.log(developers);
} catch {
  console.log("something went wrong");
}
