//Access the dataset and function to getSum of salaries based on country

const data=require("../js_drill_2.cjs")
const getSumOfSalariesBasedOnCountry=require("../problem5")

try {
    let salaryByCountry=getSumOfSalariesBasedOnCountry(data)
    console.log(salaryByCountry)
} catch (error) {
    console.log("something went wrong")
}