
//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )


function findAllWebDevelopers(data) {
    let webDevelopers = [];
    for (let index = 0; index < data.length; index++) {
        if (data[index].job === "Web Developer III" || data[index].job === "Web Developer II") {
            webDevelopers.push(data[index]);
        }
    }
    return webDevelopers;
}



module.exports=findAllWebDevelopers;