//3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function processSalaries(data){
for(let index=0;index<data.length;index++){
    const salary = parseFloat(data[index].salary.replace("$", ""));
        const correctedSalary = salary * 10000;
        data[index].corrected_salary = correctedSalary;      
}
return data
}

module.exports=processSalaries;