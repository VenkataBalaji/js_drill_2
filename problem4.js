//4. Find the sum of all salaries.

const data=require("./js_drill_2.cjs")

   function getSumOfSalaries(){
    let totalSalary=0;
    for(let index=0;index<data.length;index++){
        let salary=parseFloat(data[index].salary.replace("$",""));
        totalSalary=totalSalary+salary
    }
    return totalSalary;
}
   


module.exports=getSumOfSalaries