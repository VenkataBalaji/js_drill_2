//6. Find the average salary of based on country. ( Groupd it based on country and then find the average .

function getAvgSlariesBasedOnCountry(data){
const averageSalaryByCountry = {};
const countByCountry = {};
for (let i = 0; i < data.length; i++) {
  const salary = parseFloat(data[i].salary.replace("$", ""));
  const country = data[i].location;

  if (averageSalaryByCountry[country]) {
    averageSalaryByCountry[country] += salary;
    countByCountry[country]++;
  } else {
    averageSalaryByCountry[country] = salary;
    countByCountry[country] = 1;
  }
}

for (const country in averageSalaryByCountry) {
  averageSalaryByCountry[country] /= countByCountry[country];
}
return averageSalaryByCountry;
}


module.exports=getAvgSlariesBasedOnCountry
