 //5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum )


function getSumOfSalariesBasedOnCountry(data){
const salaryByCountry = {};
for (let i = 0; i < data.length; i++) {
  const salary = parseFloat(data[i].salary.replace("$", ""));
  const country = data[i].location;

  if (salaryByCountry[country]) {
    salaryByCountry[country] += salary;
  } else {
    salaryByCountry[country] = salary;
  }
}
return salaryByCountry
}


module.exports=getSumOfSalariesBasedOnCountry
